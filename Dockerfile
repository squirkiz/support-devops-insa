#On récupère les dépendances
FROM node:10
#On définit le dossier de travail
WORKDIR /app
#On copie le contenu du répertoire courant de la machine hote vers invité
COPY . . 
#On ouvre le port 3000, cette directive ne fait rien en fait lol (doc)
EXPOSE 3000
#Installe les dépendances nodes décrites dans package.json
RUN npm install  
#Démarre le serveur node
CMD ["npm", "start"]
#Executer le serveur node du projet et redémarrer si une source est modifée
#CMD ["./node_modules/.bin/nodemon","."]
#Utiliser fichier source de l'hote et non pas de l'image
#Si on modifie le fichier sur l'hote, la modif sera vue de façon automatique
